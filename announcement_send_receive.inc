<?php


// Author: Robert Castelo
// Support: development@cortextcommunications.com
// Info: http://www.cortextcommunications.com/development/newsletter

//
//  ********** SEND EMAIL *****************************
//

  
function enewsletter_process_publications($publications) {

  // Get Administration settings
  $unsent_count = variable_get('send_maximum', 250);
  
  // get profile field names
  if (module_exist('profile')) {
    $results = db_query("SELECT name FROM {profile_fields}");
    while ($row = db_fetch_object($results)) {
        $profile_fields[] = $row->name;
    }
  }

  foreach ($publications as $publication) {

        // Check it's time to send this publication
        // Publication time 0 means schedule is set to manual
        if ($publication->next  != 0 && $publication->next <= time()) {
        
            // is this first time publication is sent
            if ($publication->last == 0) $publication->last = $publication->start;
            if ($publication->first== 0) $publication->first = $publication->next;
        
            // Get terms and nodes
            $allowed_terms = publication_select_allowed_terms($publication->publication_id);
            $selected_terms = subscribed_get_selected_terms($publication->type, $publication->publication_id);

            // check only allowed terms are included
            $selected_terms = publication_check_terms($allowed_terms, $selected_terms);
            $nodes_by_term = publication_get_nodes($publication->publication_id, $selected_terms, $publication->last, $publication->next);
            
            // Get user info
            $subscriptions = subscribed_get_subscriptions_qued($publication->publication_id, $publication->next, $unsent_count);
            $profiles = subscribed_get_profiles($publication->publication_id, $publication->next, $unsent_count);
            // LOCK
           
           $subscriber_count = mysql_num_rows($subscriptions);

            // Check if there are subscriptions
            if ($subscriber_count > 0) {

                // Get templates for this newsletter (plain text and HTML)
                $templates = enewsletter_load_newsletter_templates($publication);
                // Encode email subject line
                //$subject = enewsletter_email_encode($publication->title);
                $site_name = check_plain(variable_get('site_name', 'drupal'));
                $newsletter->title = check_plain($publication->title);

                
                // loop through subscriptions
                while ($subscription = db_fetch_object($subscriptions)) {

                    // get nodes for this user
                    $nodes = publication_get_term_nodes($selected_terms[$subscription->uid], $nodes_by_term, $subscription->format);
                    
                    // if there are no nodes, don't send - unless it's an announcement
                    if ($nodes || $publication->type == 'announcement') {
                   
                        // process nodes into one string
                        $nodes = publication_process_nodes($nodes, $subscription->uid, $subscription->format);
                        
                        // load template for this subscription
                        $template->template = ($subscription->format == 'html') ? $templates['html'] : $templates['text'];
                        $template->format = ($subscription->format == 'html') ? $templates['html_format'] : $templates['text_format'];
                        
                        // load content for this subscription
                        $content = enewsletter_load_content($site_name, $publication, $template, $subscription, $nodes, $profiles[$subscription->uid], $profile_fields);
                        $header = enewsletter_email_header($publication->publication_id, $subscription->format, $subscription->mail);
                        
                        // convert if subscription format is plain text
                        if ($subscription->format != 'html') {
                            $converted = html2txt_convert($content, 70, TRUE, '', 3);
                            $content = $converted->text . $converted->links;
                        }

                        // Send email
                        $email_sent = enewsletter_send_email($subscription->mail, $publication->title, $content, $header);
                        
                        if ($email_sent) {

                            // make record of send
                            subscribed_insert_sent($publication->publication_id, $publication->type, $subscription->uid, $publication->next);
                            $unsent_count = $unsent_count - 1;
                            usleep(variable_get('send_delay', 2));
                        }
                        // Check maximum number of emails (per cron) have not been sent
                        // UNLOCK
                        if ($unsent_count == 0) return;
                    }
                }
                
            }
            // UNLOCK
            schedule_set_next_publication_time($publication);
        }
  }
  
  return;
}


function enewsletter_load_newsletter_templates($publication) {
    // TO DO
    $templates = templates_get_templates('enewsletter', $publication->templates);
    
    $template_text = enewsletter_get_template($publication->template_text);
    $template_html = enewsletter_get_template($publication->template_html);
    $templates = array(
        'text' => $template_text['template'], 
        'text_format' => $template_text['format'], 
        'html' => $template_html['template'], 
        'html_format' => $template_html['format']);
    return $templates;
}


//  Fill template with newsletter/user data
function enewsletter_load_content($site_name, $publication, $template, $subscription, $nodes = NULL, $profile = NULL, $profile_fields = NULL) {
  
  global $base_url;

  // Profile info
  if ($profile) {
    while (list($field, $data) = each($profile)) {
        $profile_variables .= '$' . $field . ' = \'' . addslashes($data) . '\';';
    }
  }
  
  $update_link = url($publication->type .'/preferences/' . $publication->publication_id . '/' . $subscription->hash, NULL, NULL, TRUE);
  $unsubscribe_link = url($publication->type .'/' . $subscription->hash, NULL, NULL, TRUE);

  // Load variables and values into string
  $php_variables = '<?php ';
  $php_variables .= '$site_url = \'' . $base_url . '\';';
  $php_variables .= '$site_name = \'' . $site_name . '\';';
  $$php_variables .= 'username = \'' . check_plain($subscription->name) . '\';';
  $php_variables .= '$publication_title = \'' . $publication->title . '\';';
  $php_variables .= '$nodes = \'' . addslashes($nodes) . '\';';
  $php_variables .= '$update_link = \'' . $update_link . '\';';
  $php_variables .= '$unsubscribe_link = \'' . $unsubscribe_link. '\';';
  $php_variables .= $profile_variables;
  $php_variables .= '?>';
  

  $content = check_output($php_variables . $template->template, $template->format);    
  $content = str_replace('$nodes', $nodes, $content);
  $content = str_replace('$username', $subscription->name, $content);
  $content = str_replace('$publication_title', $publication->title, $content);
  $content = str_replace('$site_url', $base_url, $content);
  $content = str_replace('$site_name', $site_name, $content);
  $content = str_replace('$update_link', $update_link, $content);
  $content = str_replace('$unsubscribe_link', $unsubscribe_link, $content);
  
  if ($profile) {
    reset($profile);
    while (list($field, $data) = each($profile)) {
        $content = str_replace('$' . $field, $data, $content);
    }
  }
  
  // Final clean up - in case user has no profile data
  if ($profile_fields) {
    while (list(, $field) = each($profile_fields)) {
        $content = str_replace('$' . $field, '', $content);
    }
  }

  // TO DO
  // Add "refer a friend" link

  return $content;
}


function enewsletter_email_header($publication_id, $format, $to) {

  $header .= "From: " . variable_get('from_address', '');
  $header .= "\n";
  $header .= "Reply-to: " . variable_get('reply_address', '');
  $header .= "\n";  
  if (variable_get('set_return_path', 0) == 1) $header .= "Return-path: " . variable_get('bounce_addresss', '') . "\n";
  $header .= "X-Mailer: Drupal";
  $header .= "\n";  
  $header .= 'X-Newsletter: ' . $publication_id;
  $header .= "\n";
  $header .= 'X-MessageId: ' . (time() + rand(1, 32767));
  $header .= "\n";
  $header .= "X-Subscriber: " . $to;
  $header .= "\n";
  $header .= "X-Date: " . time();
  $header .= "\n";
  $header .= "Errors-to: " . variable_get('bounce_addresss', '');
  $header .= "\n";
  $header .= 'MIME-Version: 1.0';
  $header .= "\n";
  if ($format == "html") {
    $header .= 'Content-Type: text/html; charset=UTF-8; Content-transfer-encoding: 8Bit'; 
  } else {
    $header .= 'Content-Type: text/plain; charset=UTF-8; format=flowed Content-transfer-encoding: 8Bit';
  }

  return $header;
}


function enewsletter_send_email($to, $subject, $content, $header) {

  if ( mail($to, $subject, $content, $header) ) {
    $sent = TRUE;
  } else {
    drupal_set_message (t("Some emails could not be sent- see log for more detail."), 'error');
    watchdog('warning', t('Email newsletter: problem sending to ') . $to . '. "' . $subject . '" ' . t('not sent'));
    $sent = FALSE;
  }
  
  return $sent;
}


function enewsletter_email_encode($string, $charset = "UTF-8") {
  /*
  ** Used to encodes mail headers that contain non US- ASCII
  ** characters.
  ** http://www.rfc-editor.org/rfc/rfc2047.txt
  **
  ** Notes:
  **   - The chunks come in groupings of 4 bytes when using base64
  **     encoded.
  **   - trim() is used to ensure that no extra spacing is added by
  **     chunk_split() or preg_replace().
  **   - Using \n as the chunk separator may cause problems on some
  **     systems and may have to be changed to \r\n or \r.
  */
  $chunk_size = 75 - 7 - strlen($charset);
  $chunk_size -= $chunk_size % 4;
  $string = trim(chunk_split(base64_encode($string), $chunk_size, "\n"));
  $string = trim(preg_replace('/^(.*)$/m', " =?$charset?B?\\1?=", $string));
  return $string;
}


?>
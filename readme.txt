********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: announcement module
Author: Robert Castelo <services at cortextcommunications dot com>
Drupal: 4.6.x
********************************************************************
DESCRIPTION:

Send one off announcement emails to registered users.

Features include: 

Personalisation, profile info is added into email.
Plain text or HTML email (subscriber chooses which to receive).
Announcement message can use Drupal input formats, e.g. use PHP to create a conditional plain text email
Bounced email handling.
Email link authentication - 'Read more' links, and account control links, allow users to access the site (based on their role) and their email subscription settings, but without accessing their full site account.
100% built for Drupal.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire announcement directory into your Drupal modules/
   directory.
   
   Add the following modules to your modules directory in the same way:

   bounced_email.module
   html2txt.module
   identity_hash.module
   publication.module
   templates.module
   schedule.module
   subscribed.module

2. Load the database definition file(s).
   
   Create the database tables by either using each module's mysql file, 
   or use the announcement_combined.mysql file instead.

   (announcement/announcement.mysql) or (announcement/announcement_combined.mysql) 
   using the tool of your choice (e.g. phpmyadmin). For mysql and command line
   access use:

     mysql -u user -p drupal < announcement/announcement.mysql
     or
     mysql -u user -p drupal < announcement/announcement_combined.mysql

   Replace 'user' with the MySQL username, and 'drupal' with the
   database being used.

3. Enable the announcement modules and other modules listed above by navigating to:

     administer > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.




********************************************************************
TO DO

  Update script for previous users
  I broke templates.module when converting it component modules, needs a little TLC to get it working again.
  Some of the configuration can be moved into a new component module handling email configuration for all modules.
  Mail que could be converted into component module.
